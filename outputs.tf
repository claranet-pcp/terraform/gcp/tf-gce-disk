output "name" {
  value = "${google_compute_disk.disk.name}"
}
output "zone" {
  value = "${google_compute_disk.disk.zone}"
}
output "size" {
  value = "${google_compute_disk.disk.size}"
}
