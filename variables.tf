variable "service" { }
variable "type" { default = "pd-standard" }
variable "zone" { }
variable "shortzone" { }
variable "size" { default = 50 }
variable "number" { }
