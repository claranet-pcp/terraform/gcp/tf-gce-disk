resource "google_compute_disk" "disk" {

  lifecycle {
    create_before_destroy = true
  }

  name = "${var.service}-${var.shortzone}-${var.number}"
  type = "${var.type}"
  zone = "${var.zone}"
  size = "${var.size}"
}
